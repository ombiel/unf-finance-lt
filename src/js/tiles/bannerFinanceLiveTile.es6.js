var {LiveTile,registerTile,getLibraries,getCredentials} = require("@ombiel/exlib-livetile-tools");
var request = require("-aek/request");
var _ = require("-aek/utils");
var _get = require("lodash.get");
var screenLink = require("@ombiel/aek-lib/screen-link");
var cssClasses = require("../../css/animations");
var financeCSS = require("../../css/finance");

//var AekStorage = require("-aek/storage");
//var storage = new AekStorage("unf-finance-lt-aek2");
var _ = require("-aek/utils");

var $;
var libReady = getLibraries(["jquery"]).then((libs)=>{
  [$] = libs;
});
const refetchPeriod = 60000;

class BannerFinanceTile extends LiveTile {

  onReady() {
    this.setOptions();

    libReady.then(()=>{
      this.renderImmediately = this.render.bind(this);

     // Debounce render so it doesn't get triggered more than once in a 1 sec period
     this.render = _.throttle(this.render.bind(this),1000);

     this.fetchData();
     this.render();

   });

  }

  fetchData(){
    var url = screenLink("unf-finance-lt/data");
    // This method is required in order for the live tile to work on native
    return getCredentials().then((creds)=>{
      this.ajax({
        url:url, data:creds, type:"POST"
      })
      .done((data)=>{
        this.balance = data.balance;
        this.render();
        this.timer(refetchPeriod,this.fetchData.bind(this));
      })
      .fail((err)=>{
        console.log(err);
      }).always(()=>{
        if(this.fetchTimer) { this.fetchTimer.stop(); }
        this.fetchTimer = this.timer(refetchPeriod,this.fetchData.bind(this));
      });
    });
  }

  render(){

   // Elements
   var faceContainer = document.createElement("div");
   var frontFace = this.frontFace = document.createElement("div");
   var button = this.financeInfo = document.createElement("div");
   //var icon = document.createElement("div");

   // Tile Attributes
   var tileAttributes = this.getTileAttributes();

   // Assign CSS classes to Elements
   $(faceContainer).addClass(financeCSS.financeFaceContainer);
   $(frontFace).addClass(financeCSS.financeFrontFace);
   $(button).addClass(financeCSS.financeButton);

   // Tile background image set in runserver.yaml
   if(tileAttributes.img || tileAttributes.image){
     var image = tileAttributes.img || tileAttributes.image;
     $(faceContainer).css("background-image",'url(' + image + ')');
   }

   this.front = true;

   // Badge
   var balanceValue;
   if(this.balance && this.balance.includes("$")){
     balanceValue = parseInt(this.balance.split('$')[1]);
   }
   if(balanceValue > 0){
     $(button).html(`<span class="${financeCSS.badgeText} ${financeCSS.important}"><i class="${financeCSS.warning} ${financeCSS.icon} " /></span>`);
     $(button).appendTo(frontFace);
   }
   $(frontFace).addClass(cssClasses.in).removeClass(cssClasses.out).css({visibility:"visible"});
   $(frontFace).appendTo(faceContainer);

   // Creates a new TileFace, adds it to the reverse face of the board it then flips the tile to bring your new content into view
   this.flipFace(faceContainer).css({
     backgroundColor:"#034879"
   });
 }

  setOptions(){
    var tileAttributes = this.getTileAttributes();
    this.initPause = parseInt(tileAttributes.initPause) || 3000;
  }

}

registerTile(BannerFinanceTile,"bannerFinanceTile");
